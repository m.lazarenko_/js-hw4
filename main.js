// Теоретичні питання

// 1. Функції скорочують код, зазвичай функція відповідає за якусь свою операцію, в яку можна підставляти різні значення, викликаючи її та вводячи різні аргументи.Також зручно, те що її можно викликати будь де. 

// 2. Аргумент потрібен для підстановки значень, які потрібно ввести у функцію, для її виконання. 

// 3. return повертає значення операції та записує його в функцію




function mathOperation(a, b, operator) {
     switch(operator) {
        case "+":
            return a + b;
            break;
        case "-":
            return a - b;
            break;
        case "/":
            return a / b;
            break;
        case "*":
            return a * b;
            break
        default:
            alert("Wrong operator") ;       
    } 
}

let number1 = +prompt("Enter the 1st number", "");
let number2 = +prompt("Enter the 2nd number", "");
let operator = prompt("Enter math operator", "");


while (isNaN(number1) || isNaN(number2) || number1 == "" || number2 == "" || operator == "") {
    number1 = +prompt("Enter the 1st number again");
    number2 = +prompt("Enter the 2nd number again");
    operator = prompt("Enter math operator again")
}


let result = mathOperation(number1, number2, operator);
console.log(`${number1} ${operator} ${number2} = ${result}`);